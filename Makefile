CXX=g++
CFLAGS=-c -Wall --std=c++11
DEBUG=

ifeq ($(OS),Windows_NT)
    RM := del
	BINARY := main.exe
else
    RM := rm
	BINARY := main
endif

all: main

debug: DEBUG = -g
debug: main

main: main.o
	$(CXX) $(CFLAGS) main.o

main.o: main.cpp
	$(CXX) $(CFLAGS) -c main.cpp
clean:
	$(RM) *.o *.gch $(BINARY)
