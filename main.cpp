#include <iostream>
#include <ctime>
#include <cstdlib>


struct Matrix {
	int *A;
	int x;
	int y;
	
	void create(int sizeX, int sizeY) {
		A = (int *) malloc(sizeX * sizeY * sizeof(int));
		x = sizeX;
		y = sizeY;
	}
	
	void zeros(int sizeX, int sizeY) {
		A = (int *) malloc(sizeX * sizeY * sizeof(int));
		x = sizeX;
		y = sizeY;
		for (int i = 0; i < y; i++){ //строка
			for (int j = 0; j < x; j++) { //столбец
				A[j + x * i] = 0;
			}
		}
	}
	
	void ones(int sizeX, int sizeY) {
		A = (int *) malloc(sizeX * sizeY * sizeof(int));
		x = sizeX;
		y = sizeY;
		for (int i = 0; i < y; i++){ //строка
			for (int j = 0; j < x; j++) { //столбец
				A[j + x * i] = 1;
			}
		}
	}
	
	void rand(int sizeX, int sizeY) {
		A = (int *) malloc(sizeX * sizeY * sizeof(int));
		x = sizeX;
		y = sizeY;
		std::srand(std::time(nullptr));
		for (int i = 0; i < y; i++) { //строка 
			for (int j = 0; j < x; j++) { //столбец
				A[j + x * i] = std::rand();
			}
		}
	}
	
	void set(int value, int cy, int cx){
		A[cx - 1 + x * (cy - 1)] = value;
	}
	
	void set(float value, int cy, int cx){
		A[cx - 1 + x * (cy - 1)] = value;
	}
	
	int get (int cy, int cx){
		return A[cx - 1 + x * (cy - 1)];
	}
	
	void display() {
		int i, j;
		for (i=0; i < y; i++){
			for (j=0; j < x; j++){
				std::cout << get(i+1, j+1) << " ";
			}
			std::cout << std::endl;
		}
	}
	
	Matrix operator + (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			Matrix res;
			res.create(x, y);
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					res.A[j + x*i] = A[j + x*i] + mat.A[j+x-i];
				}
			}
			return res;
		}
		return *this;
	}
	
	Matrix operator += (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					A[j + x*i] += mat.A[j+x-i];
				}
			}
		}
		return (*this);
	}
	
	Matrix operator - (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			Matrix res;
			res.create(x, y);
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					res.A[j + x*i] = A[j + x*i] - mat.A[j+x-i];
				}
			}
			return res;
		}
		return *this;
	}
	
	Matrix operator - () {
		Matrix res;
		res.create(x, y);
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				res.A[j + x*i] = -A[j + x*i];
			}
		}
		return res;
	}
	
	Matrix operator -= (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					A[j + x*i] -= mat.A[j+x-i];
				}
			}
		}
		return (*this);
	}
	
	Matrix operator / (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			Matrix res;
			res.create(x, y);
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					res.A[j + x*i] = A[j + x*i] / mat.A[j+x-i];
				}
			}
			return res;
		}
		return *this;
	}
	
	Matrix operator /= (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					A[j + x*i] /= mat.A[j+x-i];
				}
			}
		}
		return (*this);
	}
	
	template <typename T>
	Matrix operator / (const T mat) {
		Matrix res;
		res.create(x, y);
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				res.A[j + x*i] = A[j + x*i] / mat;
			}
		}
		return res;
	}
	
	template <typename T>
	Matrix operator /= (const T mat) {
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				A[j + x*i] /= mat;
			}
		}
		return (*this);
	}
	
	Matrix operator * (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			Matrix res;
			res.create(x, y);
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					res.A[j + x*i] = A[j + x*i] * mat.A[j+x-i];
				}
			}
			return res;
		}
		return *this;
	}
	
	Matrix operator *= (const Matrix mat) {
		if (x == mat.x && y == mat.x) {
			for (int i = 0; i < y; i++) {
				for (int j = 0; j < x; j++) {
					A[j + x*i] *= mat.A[j+x-i];
				}
			}
		}
		return (*this);
	}
	
	template <typename T>
	Matrix operator * (const T mat) {
		Matrix res;
		res.create(x, y);
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				res.A[j + x*i] = A[j + x*i] * mat;
			}
		}
		return res;
	}
	
	template <typename T>
	Matrix operator *= (const T mat) {
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				A[j + x*i] *= mat;
			}
		}
		return (*this);
	}

	float avg() {
		float res;
		float sum;
		for (int i = 0; i < y; i++){
			for (int j = 0; j < x; j++){
				sum += A[j + x*i];
			}
		}
		res = sum/(x*y);
		return res;
	}
	
	float max() {
		float res = 0;
		for (int i = 0; i < y; i++){
			for (int j = 0; j < x; j++){
				if (res < A[j + i*x]) res = A[j + i*x];
			}
		}
		return res;
	}
	
	float min() {
		float res = A[0];
		for (int i = 0; i < y; i++){
			for (int j = 0; j < x; j++){
				if (res > A[j + i*x]) res = A[j + i*x];
			}
		}
		return res;
	}
	
	float sum() {
		float res = 0;
		for (int i = 0; i < y; i++){
			for (int j = 0; j < x; j++){
				res += A[j + i*x];
			}
		}
		return res;
	}

	Matrix avg(int option) {
		Matrix res;
		float sum;
		switch (option){
			case 0: //вертикальное сокращение
				res.create(x, 1);
				for (int j = 0; j < x; j++) {
					sum = 0;
					for (int i = 0; i < y; i++){
						sum += A[j + x*i];
					}
					res.set((float)sum/y,1,j+1);
				}
				break;
			case 1:
				res.create(y, 1);
				for (int i = 0; i < y; i++) {
					sum = 0;
					for (int j = 0; j < x; j++){
						sum += A[j + x*i];
					}
					res.set((float)sum/x,1,i+1);
				}
				break;
		}				
		return res;
	}
	
	Matrix max(int option) {
		Matrix res;
		float max;
		switch (option){
			case 0: //вертикальное сокращение
				res.create(x, 1);
				for (int j = 0; j < x; j++) {
					max = 0;
					for (int i = 0; i < y; i++){
						if (max < A[j + i*x]) max = A[j+i*x];
					}
					res.set(max,1,j+1);
				}
				break;
			case 1:
				res.create(y, 1);
				for (int i = 0; i < y; i++) {
					max = 0;
					for (int j = 0; j < x; j++){
						if (max < A[j + i*x]) max = A[j+i*x];
					}
					res.set(max,1,i+1);
				}
				break;
		}				
		return res;
	}
	
	Matrix min(int option) {
		Matrix res;
		float min;
		switch (option){
			case 0: //вертикальное сокращение
				res.create(x, 1);
				for (int j = 0; j < x; j++) {
					min = A[0];
					for (int i = 0; i < y; i++){
						if (min > A[j + i*x]) min = A[j+i*x];
					}
					res.set(min,1,j+1);
				}
				break;
			case 1:
				res.create(y, 1);
				for (int i = 0; i < y; i++) {
					min = A[0];
					for (int j = 0; j < x; j++){
						if (min > A[j + i*x]) min = A[j+i*x];
					}
					res.set(min,1,i+1);
				}
				break;
		}				
		return res;
	}
	
	Matrix sum(int option) {
		Matrix res;
		float sum;
		switch (option){
			case 0: //вертикальное сокращение
				res.create(x, 1);
				for (int j = 0; j < x; j++) {
					sum = 0;
					for (int i = 0; i < y; i++){
						sum += A[j + x*i];
					}
					res.set((float)sum,1,j+1);
				}
				break;
			case 1:
				res.create(y, 1);
				for (int i = 0; i < y; i++) {
					sum = 0;
					for (int j = 0; j < x; j++){
						sum += A[j + x*i];
					}
					res.set((float)sum,1,i+1);
				}
				break;
		}				
		return res;
	}
	
	Matrix multmat (const Matrix mat) {
		if (x != mat.y) {
			std::cout << "Matrixes are not multiplicable!" << std::endl;
			return *this;	
		}
		else {
			Matrix res;
			float sum;
			res.create(x, mat.y);
			for (int i = 0; i < y; i++) { //строки первой таблицы
				for (int j = 0; j < mat.x; j++) { //столбцы второй таблицы
					sum = 0;
					for (int u = 0; u < x; u++) { //общее измерение двух таблиц
						sum += A[u + i*x]*mat.A[j + u*mat.x];
					}
					res.set(sum,i+1,j+1);
				}
			}
			return res;
		}
	}
	
	Matrix transpone () {
		Matrix trans;
		trans.create(y, x);
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				trans.set(A[j + i*x],j+1, i+1);
			}
		}
		return trans;
	}
	
	friend std::ostream& operator << (std::ostream& out, Matrix mat)
	{
		int i, j;
		for (i=0; i < mat.y; i++){
			for (j=0; j < mat.x; j++){
				out << mat.get(i+1, j+1) << " ";
			}
			out << std::endl;
		}
	return out;
	}
};

int main() {
	Matrix mat;
	Matrix mat2;
	mat.ones(4, 4);
	mat2.ones(4, 4);
	std::cout << mat << std::endl << -mat << std::endl << mat - mat2 << std::endl;
	std::cout << mat + mat2 << std::endl;
	
	mat2 *= 2;
	std::cout << "* and /\n" << mat * mat2 << std::endl << mat / mat2 << std::endl;
	
	mat.set(5, 1, 1);
	mat.set(4,1,2);
	mat.display();
	mat += mat2;
	std::cout << mat << mat.avg() << std::endl << mat.max() << std::endl << mat.min() << std::endl << mat.sum() << std::endl;
	std::cout << mat.avg(0) << std::endl << mat.avg(1);
	std::cout << mat.max(0) << std::endl << mat.max(1);
	std::cout << mat.min(0) << std::endl << mat.min(1);
	std::cout << mat.sum(0) << std::endl << mat.sum(1);
	
	mat.ones(2,2);
	mat2.ones(2,2);
	mat.set(1,1,1); mat.set(2,1,2); mat.set(3,2,1); mat.set(4,2,2);
	mat2.set(0,1,1); mat2.set(0,1,2); mat2.set(1,2,1); mat2.set(1,2,2);
	std::cout << mat << std::endl << mat2 << std::endl << mat.multmat(mat2);
	
	mat.rand(4,2);
	std::cout << mat << std::endl << mat.transpone();
	return 0; 
}